package br.com.beblue.spotifycashback.mapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class AlbumsMapper {

    @JsonProperty("items")
    private List<AlbumMapper> albums;
}
