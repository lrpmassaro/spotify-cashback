package br.com.beblue.spotifycashback.mapper;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class AlbumMapper {

    @JsonProperty("name")
    private String name;

    @JsonProperty("artists")
    private List<ArtistMapper> artists;

    public AlbumMapper(){}

    public String getName(){return name;}

    public void setName(String name){this.name = name;}

    public List<ArtistMapper> getArtists() {
        return artists;
    }

    public String getFirstArtistName (){
        return this.getArtists().isEmpty()
                ? null
                : this.getArtists().get(0).getName();
    }
}
