package br.com.beblue.spotifycashback.mapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class ArtistMapper {

    @JsonProperty("name")
    private String name;
}
