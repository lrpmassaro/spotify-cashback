package br.com.beblue.spotifycashback.model.enums;

import br.com.beblue.spotifycashback.strategy.*;
import org.springframework.context.ApplicationContext;

public enum Genre {

    POP{
        @Override
        public CashbackStrategy cashbackStrategy(ApplicationContext context){
            return context.getBean(PopGenreCashback.class);
        }
    },
    MPB{
        @Override
        public CashbackStrategy cashbackStrategy(ApplicationContext context){
            return context.getBean(MpbGenreCashback.class);
        }
    },
    CLASSIC{
        @Override
        public CashbackStrategy cashbackStrategy(ApplicationContext context){
            return context.getBean(ClassicGenreCashback.class);
        }
    },
    ROCK{
        @Override
        public CashbackStrategy cashbackStrategy(ApplicationContext context){
            return context.getBean(RockGenreCashback.class);
        }
    };

    public abstract CashbackStrategy cashbackStrategy(ApplicationContext context);

}
