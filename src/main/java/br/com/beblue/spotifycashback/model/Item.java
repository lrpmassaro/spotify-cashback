package br.com.beblue.spotifycashback.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "item")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "album_id")
    private Album album;

    @ManyToOne
    @JoinColumn(name = "sale_id")
    private Sale sale;

    private int quantity;

    private Double price;

    public Item(Long id, Album album, int quantity, Double price){
        this.id = id;
        this.album = album;
        this.quantity = quantity;
        this.price = price;
    }
}
