package br.com.beblue.spotifycashback.model;

import br.com.beblue.spotifycashback.model.enums.Genre;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Random;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "album")
public class Album implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "artist")
    private String artist;

    @Column(name = "genre")
    @Enumerated(EnumType.STRING)
    private Genre genre;

    @Column(name = "price")
    private double price;

    public Album(Long id, String name, String artist, Genre genre){
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.genre = genre;
        this.price = new Random().nextInt(99) + 1;
    }

    public Album(Long id, String name, String artist, Genre genre, double price){
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.genre = genre;
        this.price = price;
    }
}
