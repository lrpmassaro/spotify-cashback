package br.com.beblue.spotifycashback.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class Spotify {

    @JsonProperty("access_token")
    private String accessToken;
}