package br.com.beblue.spotifycashback.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "sale")
public class Sale implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "sale_date")
    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate date;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Item> items;

    @Column(name = "total_value")
    private Double totalValue = 0.0;

    @Column(name = "cashback")
    private Double cashback = 0.0;

    public Sale(LocalDate date){
        this.id = null;
        this.date = date;
        this.items = new ArrayList<>();
        this.totalValue = 0.0;
        this.cashback = 0.0;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public void sumCashback(Double cashback) {
        this.cashback += cashback;
    }

    public void sumValue(Double value) {
        this.totalValue += value;
    }

}
