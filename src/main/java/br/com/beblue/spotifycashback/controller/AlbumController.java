package br.com.beblue.spotifycashback.controller;

import br.com.beblue.spotifycashback.dto.AlbumDTO;
import br.com.beblue.spotifycashback.model.Album;
import br.com.beblue.spotifycashback.model.enums.Genre;
import br.com.beblue.spotifycashback.service.AlbumService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/albums")
public class AlbumController implements Serializable {

    private final AlbumService albumService;

    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<AlbumDTO> findById(
            @PathVariable Long id) {

        Optional<Album> album = albumService.findById(id);

        return album.map(value -> new ResponseEntity<>(
                new AlbumDTO(
                        value.getId(),
                        value.getName(),
                        value.getArtist(),
                        value.getGenre(),
                        value.getPrice()
                ),
                HttpStatus.OK)).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @GetMapping
    public ResponseEntity<Page<AlbumDTO>> findAll(
            @RequestParam(required = false) Genre genre,
            Pageable pageable) {

        Page<Album> albums;

        if (genre != null) {
            if (genre == Genre.CLASSIC || genre == Genre.MPB || genre == Genre.POP || genre == Genre.ROCK) {
                albums = albumService.findByGenreOrderByNameAsc(genre, pageable);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        } else {
            albums = albumService.findAll(pageable);
        }

        List<AlbumDTO> albumsDTO = albums
                .getContent()
                .stream()
                .map(album -> new AlbumDTO(
                        album.getId(),
                        album.getName(),
                        album.getArtist(),
                        album.getGenre(),
                        album.getPrice()
                )).collect(Collectors.toList());

        return new ResponseEntity<>(new PageImpl<>(albumsDTO), HttpStatus.OK);
    }
}
