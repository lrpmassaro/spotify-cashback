package br.com.beblue.spotifycashback.controller;

import br.com.beblue.spotifycashback.dto.ItemDTO;
import br.com.beblue.spotifycashback.dto.SaleDTO;
import br.com.beblue.spotifycashback.model.Sale;
import br.com.beblue.spotifycashback.service.SaleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1/sales")
public class SaleController {

    private SaleService saleService;

    public SaleController(SaleService saleService){
        this.saleService = saleService;
    }

    @PostMapping
    public ResponseEntity<Void> save(
            @RequestBody @Validated SaleDTO saleDto){
        this.saleService.save(saleDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SaleDTO> findById(
            @PathVariable Long id){

        Optional<Sale> sale = saleService.findById(id);

        if(sale.isPresent()){
            List<ItemDTO> itemsDTO = sale
                    .get().getItems()
                    .stream()
                    .map(item -> new ItemDTO(
                            item.getAlbum().getId(),
                            item.getAlbum().getName(),
                            item.getAlbum().getArtist(),
                            item.getAlbum().getGenre(),
                            item.getQuantity(),
                            item.getAlbum().getPrice()
                    )).collect(Collectors.toList());

            return new ResponseEntity<>(new SaleDTO(
                    sale.get().getId(),
                    sale.get().getDate(),
                    itemsDTO,
                    sale.get().getTotalValue(),
                    sale.get().getCashback()
            ),HttpStatus.OK);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @GetMapping
    public ResponseEntity<Page<SaleDTO>> findAll(
            @RequestParam (required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate firstDate,
            @RequestParam (required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate lastDate,
            Pageable pageable) {

        Page<Sale> sales;

        if (firstDate != null && lastDate != null){
            sales = saleService.findByDateBetweenOrderByDateDesc(firstDate, lastDate, pageable);
        } else {
            sales = saleService.findAll(pageable);
        }

        if (!sales.getContent().isEmpty()){
            List<SaleDTO> salesDTO = sales
                    .getContent()
                    .stream()
                    .map(sale -> new SaleDTO(
                            sale.getId(),
                            sale.getDate(),
                            sale.getItems()
                                    .stream()
                                    .map(item -> new ItemDTO(
                                            item.getAlbum().getId(),
                                            item.getAlbum().getName(),
                                            item.getAlbum().getArtist(),
                                            item.getAlbum().getGenre(),
                                            item.getQuantity(),
                                            item.getAlbum().getPrice()
                                    )).collect(Collectors.toList()),
                            sale.getTotalValue(),
                            sale.getCashback()
                    )).collect(Collectors.toList());

            return new ResponseEntity<>(new PageImpl<>(salesDTO), HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
