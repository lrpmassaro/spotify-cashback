package br.com.beblue.spotifycashback.strategy;

import br.com.beblue.spotifycashback.model.Album;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class ClassicGenreCashback implements CashbackStrategy{

    private Map<DayOfWeek, Double> percentual = createPercentual();

    public Map<DayOfWeek, Double> createPercentual(){

        Map<DayOfWeek, Double> map = new HashMap<>();

        map.put(DayOfWeek.SUNDAY, Double.valueOf(0.35));
        map.put(DayOfWeek.MONDAY, Double.valueOf(0.03));
        map.put(DayOfWeek.TUESDAY, Double.valueOf(0.05));
        map.put(DayOfWeek.WEDNESDAY, Double.valueOf(0.08));
        map.put(DayOfWeek.THURSDAY, Double.valueOf(0.13));
        map.put(DayOfWeek.FRIDAY, Double.valueOf(0.18));
        map.put(DayOfWeek.SATURDAY, Double.valueOf(0.25));

        return map;
    }

    @Override
    public Double calculateCashback(Album album, int quantity){

        Double subTotal = album.getPrice() * quantity;

        Double cashbackDayPercent = this.percentual.get(LocalDate.now().getDayOfWeek());

        Double cashback = BigDecimal.valueOf(cashbackDayPercent).multiply(BigDecimal.valueOf(subTotal)).doubleValue();

        return cashback;
    }
}
