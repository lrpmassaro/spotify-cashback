package br.com.beblue.spotifycashback.strategy;

import br.com.beblue.spotifycashback.model.Album;

public interface CashbackStrategy {

    Double calculateCashback(Album album, int quantity);
}
