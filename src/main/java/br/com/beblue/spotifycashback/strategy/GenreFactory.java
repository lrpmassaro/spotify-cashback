package br.com.beblue.spotifycashback.strategy;

import br.com.beblue.spotifycashback.model.enums.Genre;

public class GenreFactory {

    public static CashbackStrategy createGenre(Genre genre){
        switch (genre){
            case POP:
                return new PopGenreCashback();
            case CLASSIC:
                return new ClassicGenreCashback();
            case MPB:
                return new MpbGenreCashback();
            case ROCK:
                return new RockGenreCashback();
            default:
                throw new RuntimeException();
        }
    }
}
