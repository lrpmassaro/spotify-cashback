package br.com.beblue.spotifycashback.strategy;

import br.com.beblue.spotifycashback.model.Album;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class RockGenreCashback implements CashbackStrategy{

    private Map<DayOfWeek, Double> percentual = createPercentual();

    public Map<DayOfWeek, Double> createPercentual(){

        Map<DayOfWeek, Double> map = new HashMap<>();

        map.put(DayOfWeek.SUNDAY, Double.valueOf(0.40));
        map.put(DayOfWeek.MONDAY, Double.valueOf(0.10));
        map.put(DayOfWeek.TUESDAY, Double.valueOf(0.15));
        map.put(DayOfWeek.WEDNESDAY, Double.valueOf(0.15));
        map.put(DayOfWeek.THURSDAY, Double.valueOf(0.15));
        map.put(DayOfWeek.FRIDAY, Double.valueOf(0.20));
        map.put(DayOfWeek.SATURDAY, Double.valueOf(0.40));

        return map;
    }

    @Override
    public Double calculateCashback(Album album, int quantity){

        Double subTotalOrderAlbumValue = album.getPrice() * quantity;

        Double cashbackDayPercent = this.percentual.get(LocalDateTime.now().getDayOfWeek());

        Double cashback = BigDecimal.valueOf(cashbackDayPercent).multiply(BigDecimal.valueOf(subTotalOrderAlbumValue)).doubleValue();

        return cashback;
    }

}
