package br.com.beblue.spotifycashback.service;

import br.com.beblue.spotifycashback.dto.ItemDTO;
import br.com.beblue.spotifycashback.dto.SaleDTO;
import br.com.beblue.spotifycashback.model.Album;
import br.com.beblue.spotifycashback.model.Item;
import br.com.beblue.spotifycashback.model.Sale;
import br.com.beblue.spotifycashback.repository.AlbumRepository;
import br.com.beblue.spotifycashback.repository.SaleRepository;
import br.com.beblue.spotifycashback.strategy.CashbackStrategy;
import br.com.beblue.spotifycashback.strategy.GenreFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class SaleService{

    private SaleRepository saleRepository;
    private AlbumRepository albumRepository;

    public SaleService(SaleRepository saleRepository, AlbumRepository albumRepository){
        this.saleRepository = saleRepository;
        this.albumRepository = albumRepository;
    }

    public void save(SaleDTO saleDTO){

        Sale sale = new Sale(LocalDate.now());

        for (ItemDTO item: saleDTO.getItems()) {
            Album album = albumRepository.findById(item.getAlbumId())
                    .orElseThrow(()-> new RuntimeException("Album not found."));
            final CashbackStrategy cashbackStrategy = GenreFactory.createGenre(album.getGenre());
            final Double cashback = cashbackStrategy.calculateCashback(album, item.getQuantity());
            final Double value = item.getQuantity() * album.getPrice();
            sale.addItem(new Item(null, album, item.getQuantity(), album.getPrice()));
            sale.sumValue(value);
            sale.sumCashback(cashback);
        }
        saleRepository.save(sale);
    }

    public Page<Sale> findAll(Pageable pageable) {
        return saleRepository.findAll(pageable);
    }

    public Optional<Sale> findById(Long id) {
        return saleRepository.findById(id);
    }

    public Page<Sale> findByDateBetweenOrderByDateDesc(
            LocalDate firstDate,
            LocalDate lastDate,
            Pageable pageable) {
        return saleRepository.findByDateBetweenOrderByDateDesc(
                firstDate,
                lastDate,
                pageable);
    }
}
