package br.com.beblue.spotifycashback.service;

import br.com.beblue.spotifycashback.model.Album;
import br.com.beblue.spotifycashback.model.enums.Genre;
import br.com.beblue.spotifycashback.repository.AlbumRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AlbumService {

    private AlbumRepository albumRepository;

    public AlbumService (AlbumRepository albumRepository){
        this.albumRepository = albumRepository;
    }

    public void save(Album album) {
        try {
            albumRepository.save(album);
        }
        catch (Exception e){
            e.getMessage();
        }
    }

    public Page<Album> findAll(Pageable pageable) {
        return albumRepository.findAll(pageable);
    }

    public Optional<Album> findById(Long id) {
        return albumRepository.findById(id);
    }

    public Page<Album> findByGenreOrderByNameAsc(Genre genre, Pageable pageable) {
        return albumRepository.findByGenreOrderByNameAsc(genre, pageable);
    }
}
