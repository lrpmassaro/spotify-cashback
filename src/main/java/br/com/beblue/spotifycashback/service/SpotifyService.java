package br.com.beblue.spotifycashback.service;

import br.com.beblue.spotifycashback.mapper.SearchMapper;
import br.com.beblue.spotifycashback.model.Album;
import br.com.beblue.spotifycashback.model.Spotify;
import br.com.beblue.spotifycashback.model.enums.Genre;
import br.com.beblue.spotifycashback.repository.AlbumRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.Objects;

@Service
public class SpotifyService implements SpotifyIntegration {

    private AlbumRepository albumRepository;
    private static String CODE_SPOTIFY_BASE64 = "YjBjMWY1M2RiNTg4NGMwZGJkY2UyZmEyMDllYTEzNTg6NTJkNWEwM2UxMGVlNGRlNGJmYjdkNDUwZTI1ZDBjNWI=";
    private static final String URL_SPOTIFY_TOKEN = "https://accounts.spotify.com/api/token";
    private static final String URL_SPOTIFY_SEARCH_ALBUMS = "https://api.spotify.com/v1/search?type=album&limit=50&market=BR";

    public SpotifyService(AlbumRepository albumRepository){
        this.albumRepository = albumRepository;
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Override
    public void integration(){
        try{
            if (CollectionUtils.isEmpty(albumRepository.findAll())){
                getSpotifyAlbums(getSpotifyToken());
            }
        } catch (Exception e){
            e.getMessage();
        }
    }

    private String getSpotifyToken(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        httpHeaders.add("Authorization", "Basic " + CODE_SPOTIFY_BASE64);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", "client_credentials");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, httpHeaders);

        try {
            ResponseEntity<Spotify> response = restTemplate().exchange(
                    URL_SPOTIFY_TOKEN,
                    HttpMethod.POST,
                    request,
                    Spotify.class);
            return response.getBody().getAccessToken();
        } catch (HttpClientErrorException e){
            return e.getMessage();
        }
    }

    private void getSpotifyAlbums(String token){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer " + token);

        HttpEntity<String> httpEntity = new HttpEntity<>("headers", httpHeaders);

        for(Genre genre : Genre.values()){
            try {
                ResponseEntity<SearchMapper> response = restTemplate().exchange(
                        URL_SPOTIFY_SEARCH_ALBUMS + "&q=" + genre.toString(),
                        HttpMethod.GET,
                        httpEntity,
                        SearchMapper.class);

                if (response.getStatusCode().equals(HttpStatus.OK)
                        && !Objects.isNull(response.getBody())){
                    saveSpotifyAlbums(response.getBody(),genre);
                }
            } catch (HttpClientErrorException e) {
                e.getMessage();
            }
        }
    }

    @Transactional
    private void saveSpotifyAlbums(SearchMapper searchMapper, Genre genre) {
        searchMapper.getAlbums().getAlbums().stream().map((albumMapper) -> {
            Album album = new Album(
                    null,
                    albumMapper.getName(),
                    albumMapper.getFirstArtistName(),
                    genre);
            return album;
        }).forEachOrdered((album) -> {
            albumRepository.findByName(album.getName())
                    .orElseGet(()->albumRepository.save(album));
        });
    }
}
