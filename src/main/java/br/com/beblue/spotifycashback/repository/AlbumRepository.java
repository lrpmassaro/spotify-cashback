package br.com.beblue.spotifycashback.repository;

import br.com.beblue.spotifycashback.model.Album;
import br.com.beblue.spotifycashback.model.enums.Genre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Long> {

    Optional<Album> findById(Long id);

    Page<Album> findByGenreOrderByNameAsc(Genre genre, Pageable pageable);

    Page<Album> findAll(Pageable pageable);

    Optional<Album> findByName(String name);
}
