package br.com.beblue.spotifycashback.repository;

import br.com.beblue.spotifycashback.model.Sale;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface SaleRepository extends JpaRepository<Sale, Long> {

    Optional<Sale> findById(Long id);

    Page<Sale> findByDateBetweenOrderByDateDesc(
            LocalDate firstDate, LocalDate lastDate, Pageable pageable);

    Page<Sale> findAll(Pageable pageable);
}
