package br.com.beblue.spotifycashback.listener;

import br.com.beblue.spotifycashback.service.SpotifyService;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class SpotifyIntegrationListener implements ApplicationListener<ApplicationReadyEvent> {

    private final SpotifyService spotifyService;

    public SpotifyIntegrationListener(SpotifyService spotifyService){
        this.spotifyService = spotifyService;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        spotifyService.integration();
    }
}
