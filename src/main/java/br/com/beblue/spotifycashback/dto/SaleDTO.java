package br.com.beblue.spotifycashback.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Value
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@ToString
@EqualsAndHashCode
public class SaleDTO {

    Long id;
    LocalDate date;
    List<ItemDTO> items;
    Double totalValue;
    Double cashback;
}
