package br.com.beblue.spotifycashback.dto;

import br.com.beblue.spotifycashback.model.enums.Genre;
import lombok.*;

@Value
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@ToString
@EqualsAndHashCode
public class ItemDTO {

    Long albumId;
    String name;
    String artist;
    Genre genre;
    int quantity;
    Double price;
}
