package br.com.beblue.spotifycashback.dto;

import br.com.beblue.spotifycashback.model.enums.Genre;
import lombok.*;

@Value
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@ToString
@EqualsAndHashCode
public class AlbumDTO {

    Long id;
    String name;
    String artist;
    Genre genre;
    double price;
}
