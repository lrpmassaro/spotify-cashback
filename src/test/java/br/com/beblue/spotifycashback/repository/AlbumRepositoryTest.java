package br.com.beblue.spotifycashback.repository;

import br.com.beblue.spotifycashback.fixture.AlbumFixture;
import br.com.beblue.spotifycashback.model.Album;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class AlbumRepositoryTest {

    private AlbumRepository albumRepository = mock(AlbumRepository.class);

    private Album album = mock(Album.class);

    @Test
    public void save(){

        albumRepository.save(album);
        verify(albumRepository).save(album);
    }
}
