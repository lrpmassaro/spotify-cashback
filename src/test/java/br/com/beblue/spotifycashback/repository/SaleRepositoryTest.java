package br.com.beblue.spotifycashback.repository;

import br.com.beblue.spotifycashback.fixture.AlbumFixture;
import br.com.beblue.spotifycashback.fixture.SaleFixture;
import br.com.beblue.spotifycashback.model.Sale;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SaleRepositoryTest {

    private SaleRepository saleRepository = mock(SaleRepository.class);

    private Sale sale = mock(Sale.class);

    @Test
    public void save(){

        saleRepository.save(sale);
        verify(saleRepository).save(sale);
    }
}
