package br.com.beblue.spotifycashback.model;

import br.com.beblue.spotifycashback.fixture.AlbumFixture;
import br.com.beblue.spotifycashback.model.enums.Genre;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static br.com.beblue.spotifycashback.model.enums.Genre.ROCK;
import static org.junit.Assert.assertEquals;

public class AlbumTest {

    public static final Long id = 1L;
    public static final String name = "Senjutsu";
    public static final String artist = "Iron Maiden";
    public static final Genre genre = ROCK;
    public static final double price = 50.0;

    @Test
    public void newAlbum(){

        final Album album = AlbumFixture.newAlbum1();

        assertEquals(album.getId(), id);
        assertEquals(album.getName(), name);
        assertEquals(album.getArtist(), artist);
        assertEquals(album.getGenre(), genre);
        Assertions.assertEquals(album.getPrice(), price);
    }
}
