package br.com.beblue.spotifycashback.model;

import br.com.beblue.spotifycashback.fixture.SaleFixture;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static br.com.beblue.spotifycashback.model.enums.Genre.*;
import static org.junit.Assert.assertEquals;

public class SaleTest {

    public static final LocalDate date1 = LocalDate.parse("2022-04-15");
    public static final LocalDate date2 = LocalDate.parse("2022-04-20");

    public static final Album album1 = new Album(1L,"Senjutsu","Iron Maiden", ROCK,50.0);
    public static final Album album2 = new Album(2L,"Thriller","Michael Jackson", POP,80.0);

    public static final Item itemA = new Item(1L, album1,1,50.0);
    public static final Item itemB = new Item(2L, album2,1,80.0);

    public static final List<Item> items1 = Arrays.asList(itemA);
    public static final List<Item> items2 = Arrays.asList(itemA, itemB);

    public static final Sale sale1 = new Sale(1L, date1, items1,50.0,10.0);
    public static final Sale sale2 = new Sale(2L, date2, items2,130.0,9.1);

    @Test
    public void newSaleWithOneItem(){

        final Sale sale = SaleFixture.newSale1();

        assertEquals(sale.getId(), sale1.getId());
        assertEquals(sale.getDate(), sale1.getDate());
        assertEquals(sale.getItems(), sale1.getItems());
        assertEquals(sale.getTotalValue(), sale1.getTotalValue());
        assertEquals(sale.getCashback(), sale1.getCashback());
    }

    @Test
    public void newSaleWithManyItems(){

        final Sale sale = SaleFixture.newSale2();

        assertEquals(sale.getId(), sale2.getId());
        assertEquals(sale.getDate(), sale2.getDate());
        assertEquals(sale.getItems(), sale2.getItems());
        assertEquals(sale.getTotalValue(), sale2.getTotalValue());
        assertEquals(sale.getCashback(), sale2.getCashback());
    }
}
