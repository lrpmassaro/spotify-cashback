package br.com.beblue.spotifycashback.model;

import br.com.beblue.spotifycashback.fixture.ItemFixture;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static br.com.beblue.spotifycashback.model.enums.Genre.ROCK;
import static org.junit.Assert.assertEquals;

public class ItemTest {

    public static final Long id = 1L;
    public static final Album album = new Album(1L,"Senjutsu","Iron Maiden", ROCK,50.0);
    public static final int quantity = 1;
    public static final double price = 50.0;

    @Test
    public void newItem(){

        final Item item = ItemFixture.newItem1();

        assertEquals(item.getId(), id);
        assertEquals(item.getAlbum(), album);
        assertEquals(item.getQuantity(), quantity);
        Assertions.assertEquals(item.getPrice(), price);
    }
}
