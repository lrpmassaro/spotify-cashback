package br.com.beblue.spotifycashback.controller;

import br.com.beblue.spotifycashback.config.CustomJacksonObjectMapper;
import br.com.beblue.spotifycashback.fixture.SaleFixture;
import br.com.beblue.spotifycashback.service.SaleService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.util.Optional;

import static br.com.beblue.spotifycashback.model.enums.Genre.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class SaleControllerTest {

    SaleService saleService = mock(SaleService.class);

    private final MockMvc mockMvc = MockMvcBuilders
            .standaloneSetup(new SaleController(saleService))
            .setMessageConverters(new MappingJackson2HttpMessageConverter(CustomJacksonObjectMapper.configure()))
            .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
            .build();

    final Pageable pageable = PageRequest.of(0,20);
    final LocalDate firstDate = LocalDate.parse("2022-04-10");
    final LocalDate lastDate = LocalDate.parse("2022-04-20");

    @Test
    public void shouldFindSaleByIdWithSingleItem() throws Exception {

        when(saleService.findById(1L))
                .thenReturn(Optional.ofNullable(SaleFixture.newListSales().get(0)));

        mockMvc.perform(get("/v1/sales/1")
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(1L))
        .andExpect(jsonPath("$.date").value("2022-04-15"))
        .andExpect(jsonPath("$.items[0].albumId").value(1L))
        .andExpect(jsonPath("$.items[0].name").value("Senjutsu"))
        .andExpect(jsonPath("$.items[0].artist").value("Iron Maiden"))
        .andExpect(jsonPath("$.items[0].genre").value(ROCK.name()))
        .andExpect(jsonPath("$.items[0].quantity").value(1))
        .andExpect(jsonPath("$.items[0].price").value(50.0))
        .andExpect(jsonPath("$.totalValue").value(50.0))
        .andExpect(jsonPath("$.cashback").value(10.0));

        verify(saleService).findById(1L);
    }

    @Test
    public void shouldFindSaleByIdWithManyItems() throws Exception {

        when(saleService.findById(2L))
                .thenReturn(Optional.ofNullable(SaleFixture.newListSales().get(1)));

        mockMvc.perform(get("/v1/sales/2")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(2L))
                .andExpect(jsonPath("$.date").value("2022-04-20"))
                .andExpect(jsonPath("$.items[0].albumId").value(1L))
                .andExpect(jsonPath("$.items[0].name").value("Senjutsu"))
                .andExpect(jsonPath("$.items[0].artist").value("Iron Maiden"))
                .andExpect(jsonPath("$.items[0].genre").value(ROCK.name()))
                .andExpect(jsonPath("$.items[0].quantity").value(1))
                .andExpect(jsonPath("$.items[0].price").value(50.0))
                .andExpect(jsonPath("$.items[1].albumId").value(2L))
                .andExpect(jsonPath("$.items[1].name").value("Thriller"))
                .andExpect(jsonPath("$.items[1].artist").value("Michael Jackson"))
                .andExpect(jsonPath("$.items[1].genre").value(POP.name()))
                .andExpect(jsonPath("$.items[1].quantity").value(1))
                .andExpect(jsonPath("$.items[1].price").value(80.0))
                .andExpect(jsonPath("$.totalValue").value(130.0))
                .andExpect(jsonPath("$.cashback").value(9.1));

        verify(saleService).findById(2L);
    }

    @Test
    public void shouldFindAllSalesPageable() throws Exception {

        when(saleService.findAll(pageable))
                .thenReturn(new PageImpl<>(SaleFixture.newListSales()));

        mockMvc.perform(get("/v1/sales")
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content[0].id").value(1L))
        .andExpect(jsonPath("$.content[0].date").value("2022-04-15"))
        .andExpect(jsonPath("$.content[0].items[0].albumId").value(1L))
        .andExpect(jsonPath("$.content[0].items[0].name").value("Senjutsu"))
        .andExpect(jsonPath("$.content[0].items[0].artist").value("Iron Maiden"))
        .andExpect(jsonPath("$.content[0].items[0].genre").value(ROCK.name()))
        .andExpect(jsonPath("$.content[0].items[0].quantity").value(1))
        .andExpect(jsonPath("$.content[0].items[0].price").value(50.0))
        .andExpect(jsonPath("$.content[0].totalValue").value(50.0))
        .andExpect(jsonPath("$.content[0].cashback").value(10.0))
        .andExpect(jsonPath("$.content[1].id").value(2L))
        .andExpect(jsonPath("$.content[1].date").value("2022-04-20"))
        .andExpect(jsonPath("$.content[1].items[0].albumId").value(1L))
        .andExpect(jsonPath("$.content[1].items[0].name").value("Senjutsu"))
        .andExpect(jsonPath("$.content[1].items[0].artist").value("Iron Maiden"))
        .andExpect(jsonPath("$.content[1].items[0].genre").value(ROCK.name()))
        .andExpect(jsonPath("$.content[1].items[0].quantity").value(1))
        .andExpect(jsonPath("$.content[1].items[0].price").value(50.0))
        .andExpect(jsonPath("$.content[1].items[1].albumId").value(2L))
        .andExpect(jsonPath("$.content[1].items[1].name").value("Thriller"))
        .andExpect(jsonPath("$.content[1].items[1].artist").value("Michael Jackson"))
        .andExpect(jsonPath("$.content[1].items[1].genre").value(POP.name()))
        .andExpect(jsonPath("$.content[1].items[1].quantity").value(1))
        .andExpect(jsonPath("$.content[1].items[1].price").value(80.0))
        .andExpect(jsonPath("$.content[1].totalValue").value(130.0))
        .andExpect(jsonPath("$.content[1].cashback").value(9.1))
        .andExpect(jsonPath("$.content[2].id").value(3L))
        .andExpect(jsonPath("$.content[2].date").value("2022-04-25"))
        .andExpect(jsonPath("$.content[2].items[0].albumId").value(2L))
        .andExpect(jsonPath("$.content[2].items[0].name").value("Thriller"))
        .andExpect(jsonPath("$.content[2].items[0].artist").value("Michael Jackson"))
        .andExpect(jsonPath("$.content[2].items[0].genre").value(POP.name()))
        .andExpect(jsonPath("$.content[2].items[0].quantity").value(1))
        .andExpect(jsonPath("$.content[2].items[0].price").value(80.0))
        .andExpect(jsonPath("$.content[2].items[1].albumId").value(3L))
        .andExpect(jsonPath("$.content[2].items[1].name").value("Novo Millennium"))
        .andExpect(jsonPath("$.content[2].items[1].artist").value("Alceu Valença"))
        .andExpect(jsonPath("$.content[2].items[1].genre").value(MPB.name()))
        .andExpect(jsonPath("$.content[2].items[1].quantity").value(2))
        .andExpect(jsonPath("$.content[2].items[1].price").value(40.0))
        .andExpect(jsonPath("$.content[2].totalValue").value(160.0))
        .andExpect(jsonPath("$.content[2].cashback").value(9.6));

        verify(saleService).findAll(pageable);
    }

    @Test
    public void shouldFindSalesByDateBetweenPageable() throws Exception {

        when(saleService.findByDateBetweenOrderByDateDesc(firstDate, lastDate, pageable))
                .thenReturn(new PageImpl<>(SaleFixture.newListSales()));

        mockMvc.perform(get("/v1/sales?firstDate=10/04/2022&lastDate=20/04/2022")
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content[0].id").value(1L))
        .andExpect(jsonPath("$.content[0].date").value("2022-04-15"))
        .andExpect(jsonPath("$.content[0].items[0].albumId").value(1L))
        .andExpect(jsonPath("$.content[0].items[0].name").value("Senjutsu"))
        .andExpect(jsonPath("$.content[0].items[0].artist").value("Iron Maiden"))
        .andExpect(jsonPath("$.content[0].items[0].genre").value(ROCK.name()))
        .andExpect(jsonPath("$.content[0].items[0].quantity").value(1))
        .andExpect(jsonPath("$.content[0].items[0].price").value(50.0))
        .andExpect(jsonPath("$.content[0].totalValue").value(50.0))
        .andExpect(jsonPath("$.content[0].cashback").value(10.0))
        .andExpect(jsonPath("$.content[1].id").value(2L))
        .andExpect(jsonPath("$.content[1].date").value("2022-04-20"))
        .andExpect(jsonPath("$.content[1].items[0].albumId").value(1L))
        .andExpect(jsonPath("$.content[1].items[0].name").value("Senjutsu"))
        .andExpect(jsonPath("$.content[1].items[0].artist").value("Iron Maiden"))
        .andExpect(jsonPath("$.content[1].items[0].genre").value(ROCK.name()))
        .andExpect(jsonPath("$.content[1].items[0].quantity").value(1))
        .andExpect(jsonPath("$.content[1].items[0].price").value(50.0))
        .andExpect(jsonPath("$.content[1].items[1].albumId").value(2L))
        .andExpect(jsonPath("$.content[1].items[1].name").value("Thriller"))
        .andExpect(jsonPath("$.content[1].items[1].artist").value("Michael Jackson"))
        .andExpect(jsonPath("$.content[1].items[1].genre").value(POP.name()))
        .andExpect(jsonPath("$.content[1].items[1].quantity").value(1))
        .andExpect(jsonPath("$.content[1].items[1].price").value(80.0))
        .andExpect(jsonPath("$.content[1].totalValue").value(130.0))
        .andExpect(jsonPath("$.content[1].cashback").value(9.1));

        verify(saleService).findByDateBetweenOrderByDateDesc(firstDate, lastDate, pageable);
    }
}
