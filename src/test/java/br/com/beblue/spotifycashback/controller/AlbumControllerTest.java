package br.com.beblue.spotifycashback.controller;

import br.com.beblue.spotifycashback.fixture.AlbumFixture;
import br.com.beblue.spotifycashback.service.AlbumService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static br.com.beblue.spotifycashback.model.enums.Genre.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class AlbumControllerTest {

    AlbumService albumService = mock(AlbumService.class);

    private final MockMvc mockMvc = MockMvcBuilders
            .standaloneSetup(new AlbumController(albumService))
            .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
            .build();

    @Test
    public void shouldFindAlbumById() throws Exception {

        when(albumService.findById(1L))
                .thenReturn(Optional.of(AlbumFixture.newAlbum1()));

        mockMvc.perform(get("/v1/albums/1")
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(1L))
        .andExpect(jsonPath("$.name").value("Senjutsu"))
        .andExpect(jsonPath("$.artist").value("Iron Maiden"))
        .andExpect(jsonPath("$.genre").value("ROCK"))
        .andExpect(jsonPath("$.price").value(50.0));

        verify(albumService).findById(1L);
    }

    @Test
    public void shouldFindAllAlbums() throws Exception {
        Pageable pageable = PageRequest.of(0,20);

        when(albumService.findAll(pageable))
                .thenReturn(new PageImpl<>(AlbumFixture.newListAlbums()));

        mockMvc.perform(get("/v1/albums?pageNumber=0&pageSize=20")
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content[0].id").value(1L))
        .andExpect(jsonPath("$.content[0].name").value("Senjutsu"))
        .andExpect(jsonPath("$.content[0].artist").value("Iron Maiden"))
        .andExpect(jsonPath("$.content[0].genre").value(ROCK.name()))
        .andExpect(jsonPath("$.content[0].price").value(50))
        .andExpect(jsonPath("$.content[1].id").value(2L))
        .andExpect(jsonPath("$.content[1].name").value("Thriller"))
        .andExpect(jsonPath("$.content[1].artist").value("Michael Jackson"))
        .andExpect(jsonPath("$.content[1].genre").value(POP.name()))
        .andExpect(jsonPath("$.content[1].price").value(80));

        verify(albumService).findAll(pageable);
    }
}
