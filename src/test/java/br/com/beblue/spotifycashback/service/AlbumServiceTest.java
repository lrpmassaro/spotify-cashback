package br.com.beblue.spotifycashback.service;

import br.com.beblue.spotifycashback.fixture.AlbumFixture;
import br.com.beblue.spotifycashback.model.Album;
import br.com.beblue.spotifycashback.repository.AlbumRepository;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class AlbumServiceTest {

    private AlbumRepository albumRepository = mock(AlbumRepository.class);

    private AlbumService albumService = new AlbumService(albumRepository);

    @Test
    public void getAlbum(){

        given(albumRepository.findById(1L))
                .willReturn(Optional.of(AlbumFixture.newAlbum1()));

        Optional<Album> album = albumService.findById(1L);

        assertEquals(album.get().getId(), AlbumFixture.newAlbum1().getId());
    }

    @Test
    public void getAlbums(){

        Pageable pageable = PageRequest.of(0,20);

        given(albumRepository.findAll(pageable))
                .willReturn(new PageImpl<>(AlbumFixture.newListAlbums()));

        Page<Album> albums = albumService.findAll(pageable);

        assertEquals(albums.getContent(), AlbumFixture.newListAlbums());
    }


}