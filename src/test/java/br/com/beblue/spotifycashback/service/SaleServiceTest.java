package br.com.beblue.spotifycashback.service;

import br.com.beblue.spotifycashback.fixture.SaleFixture;
import br.com.beblue.spotifycashback.model.Sale;
import br.com.beblue.spotifycashback.repository.AlbumRepository;
import br.com.beblue.spotifycashback.repository.SaleRepository;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class SaleServiceTest {

    private SaleRepository saleRepository = mock(SaleRepository.class);
    private AlbumRepository albumRepository = mock(AlbumRepository.class);

    private SaleService saleService = new SaleService(saleRepository, albumRepository);

    final Pageable pageable = PageRequest.of(0,20);
    final LocalDate firstDate = LocalDate.parse("2022-04-10");
    final LocalDate lastDate = LocalDate.parse("2022-04-20");

    @Test
    public void getSale(){

        given(saleRepository.findById(1L))
                .willReturn(Optional.of(SaleFixture.newSale1()));

        Optional<Sale> sale = saleService.findById(1L);

        assertEquals(sale.get().getId(), SaleFixture.newSale1().getId());
    }

    @Test
    public void getSales(){

        given(saleRepository.findAll(pageable))
                .willReturn(new PageImpl<>(SaleFixture.newListSales()));

        Page<Sale> sales = saleService.findAll(pageable);

        assertEquals(sales.getContent(), SaleFixture.newListSales());
    }

    @Test
    public void getSalesBetweenDates(){

        given(saleRepository.findByDateBetweenOrderByDateDesc(firstDate, lastDate, pageable))
                .willReturn(new PageImpl<>(SaleFixture.newListSales()));

        Page<Sale> sales = saleService.findByDateBetweenOrderByDateDesc(firstDate, lastDate, pageable);

        assertEquals(sales.getContent(), SaleFixture.newListSales());
    }
}
