package br.com.beblue.spotifycashback;

import br.com.beblue.spotifycashback.model.Album;
import br.com.beblue.spotifycashback.fixture.AlbumFixture;
import br.com.beblue.spotifycashback.model.Item;
import br.com.beblue.spotifycashback.fixture.ItemFixture;
import br.com.beblue.spotifycashback.model.enums.Genre;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractTest {

    public static final Long id1 = 1L;
    public static final Long id2 = 2L;
    public static final Long id3 = 3L;

    public static final String name1 = "Senjutsu";
    public static final String name2 = "Thriller";
    public static final String name3 = "Novo Millennium";

    public static final String artist1 = "Iron Maiden";
    public static final String artist2 = "Michael Jackson";
    public static final String artist3 = "Alceu Valença";

    public static final Genre genreRock = Genre.ROCK;
    public static final Genre genrePop = Genre.POP;
    public static final Genre genreMpb = Genre.MPB;

    public static final double price1 = 50.0;
    public static final double price2 = 80.0;
    public static final double price3 = 40.0;

    public static final int quantity1 = 1;
    public static final int quantity2 = 2;

    public static final Album album1 = AlbumFixture.newAlbum1();
    public static final Album album2 = AlbumFixture.newAlbum2();
    public static final Album album3 = AlbumFixture.newAlbum3();

    public static final Item itemA = ItemFixture.newItem1();
    public static final Item itemB = ItemFixture.newItem2();
    public static final Item itemC = ItemFixture.newItem3();

    public static final LocalDate date1 = LocalDate.parse("2022-04-15");
    public static final LocalDate date2 = LocalDate.parse("2022-04-20");
    public static final LocalDate date3 = LocalDate.parse("2022-04-25");

    public static final List<Item> items1 = Arrays.asList(itemA);
    public static final List<Item> items2 = Arrays.asList(itemA, itemB);
    public static final List<Item> items3 = Arrays.asList(itemB, itemC);

    public static final double totalValue1 = itemA.getQuantity() * itemA.getPrice();
    public static final double totalValue2 = (
            itemA.getQuantity() * itemA.getPrice()) + (itemB.getQuantity() * itemB.getPrice());
    public static final double totalValue3 = (
            itemB.getQuantity() * itemB.getPrice()) + (itemC.getQuantity() * itemC.getPrice());

    public static final double totalCashback1 = 10.0;
    public static final double totalCashback2 = 9.1;
    public static final double totalCashback3 = 9.6;
}
