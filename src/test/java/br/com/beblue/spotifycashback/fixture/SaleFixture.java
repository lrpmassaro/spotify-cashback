package br.com.beblue.spotifycashback.fixture;

import br.com.beblue.spotifycashback.AbstractTest;
import br.com.beblue.spotifycashback.model.Sale;

import java.util.Arrays;
import java.util.List;

public class SaleFixture {

    public static Sale newSale1(){
        return new Sale(
                AbstractTest.id1,
                AbstractTest.date1,
                AbstractTest.items1,
                AbstractTest.totalValue1,
                AbstractTest.totalCashback1
        );
    }

    public static Sale newSale2(){
        return new Sale(
                AbstractTest.id2,
                AbstractTest.date2,
                AbstractTest.items2,
                AbstractTest.totalValue2,
                AbstractTest.totalCashback2
        );
    }

    public static Sale newSale3(){
        return new Sale(
                AbstractTest.id3,
                AbstractTest.date3,
                AbstractTest.items3,
                AbstractTest.totalValue3,
                AbstractTest.totalCashback3
        );
    }

    public static List<Sale> newListSales(){
        return Arrays.asList(
                newSale1(),
                newSale2(),
                newSale3()
        );
    }
}