package br.com.beblue.spotifycashback.fixture;

import br.com.beblue.spotifycashback.AbstractTest;
import br.com.beblue.spotifycashback.model.Album;

import java.util.Arrays;
import java.util.List;

public class AlbumFixture {

    public static Album newAlbum1(){
        return new Album(
                AbstractTest.id1,
                AbstractTest.name1,
                AbstractTest.artist1,
                AbstractTest.genreRock,
                AbstractTest.price1
        );
    }

    public static Album newAlbum2(){
        return new Album(
                AbstractTest.id2,
                AbstractTest.name2,
                AbstractTest.artist2,
                AbstractTest.genrePop,
                AbstractTest.price2
        );
    }

    public static Album newAlbum3(){
        return new Album(
                AbstractTest.id3,
                AbstractTest.name3,
                AbstractTest.artist3,
                AbstractTest.genreMpb,
                AbstractTest.price3
        );
    }

    public static List<Album> newListAlbums(){
        return Arrays.asList(
                newAlbum1(),
                newAlbum2()
        );
    }
}
