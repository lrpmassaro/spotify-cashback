package br.com.beblue.spotifycashback.fixture;

import br.com.beblue.spotifycashback.AbstractTest;
import br.com.beblue.spotifycashback.model.Item;

public class ItemFixture {

    public static Item newItem1(){
        return new Item(
                AbstractTest.id1,
                AbstractTest.album1,
                AbstractTest.quantity1,
                AbstractTest.price1
        );
    }

    public static Item newItem2(){
        return new Item(
                AbstractTest.id2,
                AbstractTest.album2,
                AbstractTest.quantity1,
                AbstractTest.price2
        );
    }

    public static Item newItem3(){
        return new Item(
                AbstractTest.id3,
                AbstractTest.album3,
                AbstractTest.quantity2,
                AbstractTest.price3
        );
    }
}
