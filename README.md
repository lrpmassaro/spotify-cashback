# E-commerce of vinyl records with cashback

## What is this project?

It's a project to register vinyl albums sales with cashback, calculated by album genre and week day, using Spotify API integration to load the albums catalog 

* Create albums sales.
* Albums catalog, paginated, filtered by genre and/or id.
* List of all sales, paginated, filtered by id and/or range of dates.

## How to run?

To run the application, you will need to install the following software:<br>
[Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) <br>
[Spring Tool Suite 4](https://spring.io/tools) ou [IntelliJ](https://www.jetbrains.com/idea/download/)<br>
[pgAdmin](https://www.pgadmin.org/)

## Environment setting:

* Clone project: `https://bitbucket.org/lrpmassaro/spotify-cashback`
* Open pgAdmin and create database with name "ecommerce".
* Open project with Spring Tools Suite or IntelliJ and run the project.

## Documentation

- [Swagger2](http://localhost:8080/)

## Endpoints

### Albums

1) `/v1/albums`. Http method: GET.<br>
   Finds a paginated list of all albums.<br>
   [http://localhost:8080/v1/albums](http://localhost:8080/v1/albums)
   
Param:<br>
`?genre=`: filters the albums list by a specific genre.<br>
Example: [http://localhost:8080/v1/albums?genre=ROCK](http://localhost:8080/v1/albums?genre=rock)

2) `/v1/albums/{id}` Http method: GET.<br>
   Finds a specific album by its identifier.<br>
   Example: [http://localhost:8080/v1/albums/1](http://localhost:8080/v1/albums/1)

### Sales

1) `/v1/sales`. Http method: GET.<br>
   Finds a paginated list of all sales.
   [http://localhost:8080/v1/sales](http://localhost:8080/v1/sales)

Params:<br>
   `?firstDate=dd/MM/yyyy&lastDate=dd/MM/yyyy` <br>
   Filters the sales by a range of dates.<br>
   Example: [http://localhost:8080/v1/sales?firstDate=10/04/2022&lastDate=20/04/2022](http://localhost:8080/v1/sales?firstDate=10/04/2022&lastDate=11/04/2022)

2) `/v1/sales/{id}` Http method: GET.<br>
   Finds a specific sale by its identifier.<br>
   Example: [http://localhost:8080/v1/sales/1](http://localhost:8080/v1/sales/1)


3) `/v1/sales` Http method: POST. <br>
   Save a sale. It is necessary to send the album id and the quantity on the request body.
   Example:
```
{
    "items": [
        {
            "albumId":1,
            "quantity":1
        },
        {
            "albumId":2,
            "quantity":2
        }
    ]
}
```

## Technologies used:
* Language [Java 8](https://www.oracle.com/br/java/)
* Database [PostgreSQL](https://www.postgresql.org/)
* Framework [Spring Boot](https://spring.io/projects/spring-boot)
* Dependency management [Gradle](https://gradle.org/)